-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Ven 11 Septembre 2015 à 15:42
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.5.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `tst`
--

-- --------------------------------------------------------

--
-- Structure de la table `match`
--

CREATE TABLE IF NOT EXISTS `match` (
  `idmatch` int(11) NOT NULL AUTO_INCREMENT,
  `equipe1` varchar(50) NOT NULL,
  `equipe2` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `stade` varchar(50) NOT NULL,
  PRIMARY KEY (`idmatch`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Contenu de la table `match`
--

INSERT INTO `match` (`idmatch`, `equipe1`, `equipe2`, `date`, `stade`) VALUES
(1, 'Angleterre', 'Fidji', '2015-09-18', 'Londres'),
(2, 'Tonga', 'Georgie', '2015-09-19', 'Gloucester'),
(3, 'Irlande', 'Canada', '2015-09-19', 'Cardiff'),
(4, 'Afrique Du Sud	', 'Japon', '2015-09-19', 'Brighton'),
(5, 'France', 'Italie', '2015-09-19', 'Londres'),
(6, 'Samoa', 'Etats-unis', '2015-09-20', 'Brighton'),
(7, 'Galles', 'Uruguay', '2015-09-20', 'Cardiff'),
(8, 'Nouvelle-zelande', 'Argentine', '2015-09-20', 'Londres'),
(9, 'Ecosse', 'Japon', '2015-09-23', 'Gloucester'),
(10, 'Australie', 'Fidji', '2015-09-23', 'Cardiff'),
(11, 'France', 'Roumanie', '2015-09-23', 'Londres'),
(12, 'Nouvelle-zelande', 'Namibie', '2015-09-24', 'Londres'),
(13, 'Argentine', 'Georgie', '2015-09-25', 'Gloucester'),
(14, 'Italie', 'Canada', '2015-09-26', 'Leeds'),
(15, 'Afrique Du Sud', 'Samoa', '2015-09-26', 'Birmingham'),
(16, 'Angleterre', 'Galles', '2015-09-26', 'Londres'),
(17, 'Australie', 'Uruguay', '2015-09-27', 'Birmingham'),
(18, 'Ecosse', 'Etats-unis', '2015-09-27', 'Leeds'),
(19, 'Irlande', 'Roumanie', '2015-09-27', 'Londres'),
(20, 'Tonga', 'Namibie', '2015-09-29', 'Exeter'),
(21, 'Galles', 'Fidji', '2015-10-01', 'Cardiff'),
(22, 'France', 'Canada', '2015-10-01', 'Milton Keynes'),
(23, 'Nouvelle-zelande', 'Georgie', '2015-10-02', 'Cardiff'),
(24, 'Samoa', 'Japon', '2015-10-03', 'Milton Keynes'),
(25, 'Afrique Du Sud', 'Ecosse', '2015-10-03', 'Newcastle'),
(26, 'Angleterre', 'Australie', '2015-10-03', 'Londres'),
(27, 'Argentine', 'Tonga', '2015-10-04', 'Leicester'),
(28, 'Irlande', 'Italie', '2015-10-04', 'Londres'),
(29, 'Canada', 'Roumanie', '2015-10-06', 'Leicester'),
(30, 'Fidji', 'Uruguay', '2015-10-06', 'Milton Keynes'),
(31, 'Afrique Du Sud', 'Etats-unis', '2015-10-07', 'Londres'),
(32, 'Namibie', 'Georgie', '2015-10-07', 'Exeter'),
(33, 'Nouvelle-zelande', 'Tonga', '2015-10-09', 'Newcastle'),
(34, 'Samoa', 'Ecosse', '2015-10-10', 'Newcastle'),
(35, 'Australie', 'Galles', '2015-10-10', 'Londres'),
(36, 'Angleterre', 'Uruguay', '2015-10-10', 'Manchester'),
(37, 'Argentine', 'Namibie', '2015-10-11', 'Leicester'),
(38, 'Italie', 'Roumanie', '2015-10-11', 'Exeter'),
(39, 'France', 'Irlande', '2015-10-11', 'Cardiff'),
(40, 'Etats-unis', 'Japon', '2015-10-11', 'Gloucester');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
